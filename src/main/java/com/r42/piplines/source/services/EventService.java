package com.r42.piplines.source.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.r42.piplines.source.model.Event;
import com.r42.piplines.source.model.IOTEvent;
import com.r42.piplines.source.mongo.repositories.EventRepository;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public void saveEvent(IOTEvent event) {
		eventRepository.save(event);
	}
	
}
