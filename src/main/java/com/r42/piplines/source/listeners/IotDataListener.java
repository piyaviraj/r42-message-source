package com.r42.piplines.source.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.r42.piplines.source.model.Event;
import com.r42.piplines.source.model.IOTEvent;
import com.r42.piplines.source.services.EventService;

@Component
public class IotDataListener {

	@Autowired
	private EventService eventService;
	
	@KafkaListener(
		    topics = "iot-data", 
		    groupId = "output-group-1",
		    containerFactory = "kafkaListenerContainerEventsFactory")
	public void listenToIotDataTopic(Event message) {

		eventService.saveEvent(IOTEvent.getIOTEvent(message));

	}
	
}
