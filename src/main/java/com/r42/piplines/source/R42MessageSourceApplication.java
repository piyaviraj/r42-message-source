package com.r42.piplines.source;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class R42MessageSourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(R42MessageSourceApplication.class, args);
	}

}
