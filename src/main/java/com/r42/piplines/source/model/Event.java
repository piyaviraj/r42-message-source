package com.r42.piplines.source.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class Event {

	private Long id;
	
	private BigDecimal value;
	
	private OffsetDateTime timestamp;
	
	private String type;
	
	private String name;
	
	private Long clusterId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public OffsetDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(OffsetDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getClusterId() {
		return clusterId;
	}

	public void setClusterId(Long clusterId) {
		this.clusterId = clusterId;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", value=" + value + ", timestamp=" + timestamp + ", type=" + type + ", name=" + name
				+ ", clusterId=" + clusterId + "]";
	}
	
	
}
