package com.r42.piplines.source.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

@Document(collection = "iot_events")
public class IOTEvent {

	private Long eventId;
	
	@Field(targetType = FieldType.DECIMAL128) 
	private BigDecimal value;
	
	private OffsetDateTime timestamp;
	
	private String type;
	
	private String name;
	
	private Long clusterId;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public OffsetDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(OffsetDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getClusterId() {
		return clusterId;
	}

	public void setClusterId(Long clusterId) {
		this.clusterId = clusterId;
	}

	public static IOTEvent getIOTEvent(Event event) {
		IOTEvent iotEvent = new IOTEvent();
		iotEvent.setEventId(event.getId());
		iotEvent.setClusterId(event.getClusterId()); 
		iotEvent.setName(event.getName()); 
		iotEvent.setTimestamp(event.getTimestamp()); 
		iotEvent.setType(event.getType()); 
		iotEvent.setValue(event.getValue()); 
		return iotEvent;
	}
	
	@Override
	public String toString() {
		return "IOTEvent [eventId=" + eventId + ", value=" + value + ", timestamp=" + timestamp + ", type=" + type + ", name=" + name
				+ ", clusterId=" + clusterId + "]";
	}
}
