package com.r42.piplines.source.configurations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import com.r42.piplines.source.mongo.OffsetDataTimeReadConverter;
import com.r42.piplines.source.mongo.OffsetDataTimeWriteConverter;

@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {
	
	@Value("${spring.data.mongodb.database}")
	private String dbName;

	private final List<Converter<?, ?>> converters = new ArrayList<>();
	
	@Override
	protected String getDatabaseName() {
		return this.dbName;
	}

	@Override
    public MongoCustomConversions customConversions() {
        converters.add(new OffsetDataTimeReadConverter());
        converters.add(new OffsetDataTimeWriteConverter());
        return new MongoCustomConversions(converters);
    }
	
}
