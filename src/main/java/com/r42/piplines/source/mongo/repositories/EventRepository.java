package com.r42.piplines.source.mongo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.r42.piplines.source.model.Event;
import com.r42.piplines.source.model.IOTEvent;

@Repository
public interface EventRepository extends MongoRepository<IOTEvent, String> {

}
