# r42-message-source

The message source service is responsible of saving Kafka broker incoming messages to the MongoDB for query purposes

This service will be replaced with the Kafka MongoDB connector. 

Currently there is a problem with the incoming data format on parsing as a JSON. Therefore, this service was developed to perform custom intervention on formating the data.

Once the formating is able to handle at the Connector level, the pipline can be streamlined with Kafka connector ecosystem and deprecate this service. 